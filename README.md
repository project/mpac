# Multi-path autocomplete

Multi-path autocomplete (formely known as Menu path autocomplete) changes some input field for entering paths to an autocomplete text field so users do not need to know the internal system path but simply can enter the title of the page to link to.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/mpac).

To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/mpac).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Configure Multi-path autocomplete:
Home >> Administration >> Configuration >>  Search and metadata
(/admin/config/search/mpac)


## Maintainers

- Stefan Borchert - [stBorchert](https://www.drupal.org/u/stborchert)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
