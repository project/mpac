<?php

namespace Drupal\mpac\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\mpac\MpacAutocomplete;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller routines for mpac autocomplete routes.
 */
class MpacAutocompleteController extends ControllerBase {

  /**
   * The mpac autocomplete helper class to find matching items.
   *
   * @var \Drupal\mpac\MpacAutocomplete
   */
  protected $mpacAutocomplete;

  /**
   * Constructs an MpacAutocompleteController object.
   *
   * @param \Drupal\mpac\MpacAutocomplete $mpac_autocomplete
   *   The mpac autocomplete helper class to find matching items.
   */
  public function __construct(MpacAutocomplete $mpac_autocomplete) {
    $this->mpacAutocomplete = $mpac_autocomplete;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('mpac.autocomplete')
    );
  }

  /**
   * Returns response for the mpac autocompletion.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request object containing the search string.
   * @param string $type
   *   The type of data to find (i.e. "path" or "shortcut").
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response containing the autocomplete suggestions
   *   for existing users.
   *
   * @see MpacAutocomplete::getMatches()
   */
  public function autocompleteItems(Request $request, $type) {
    $matches = $this->mpacAutocomplete->getMatches($type, $request->query->get('q'));

    return new JsonResponse($matches);
  }

}
