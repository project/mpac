<?php

namespace Drupal\mpac\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure user settings for this site.
 */
class AutocompleteSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mpac_autocomplete_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'mpac.autocomplete',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('mpac.autocomplete');

    $form['mpac_max_items'] = [
      '#type' => 'select',
      '#title' => $this->t('Maximum number of items'),
      '#description' => $this->t('Select the maximum number of items in an autocomplete list provided by %mpac.', ['%mpac' => 'Multi-path autocomplete']),
      '#default_value' => $config->get('items.max'),
      '#options' => array_combine([10, 20, 50, 100], [10, 20, 50, 100]),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('mpac.autocomplete')
      ->set('items', ['max' => (int) $form_state->getValue('mpac_max_items')])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
