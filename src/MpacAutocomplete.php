<?php

namespace Drupal\mpac;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Defines a helper class to get mpac autocompletion results.
 */
class MpacAutocomplete {

  /**
   * The config factory to get the anonymous user name.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a MpacAutocomplete object.
   */
  public function __construct(ConfigFactory $config_factory, ModuleHandlerInterface $module_handler) {
    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Get matches for the autocompletion.
   *
   * @param string $type
   *   The type of data to find (i.e. "path" or "shortcut").
   * @param string $string
   *   The string to match.
   *
   * @return array
   *   An array containing the matching items.
   */
  public function getMatches($type, $string) {
    $matches = [];

    $handlers = mpac_get_selection_handlers($type);
    $limit = 10;
    if ($string) {
      $limit = $this
        ->configFactory
        ->get('mpac.autocomplete')
        ->get('items.max');
      // Load results.
      foreach ($handlers as $handler) {
        $matches = array_merge($matches, $handler->getMatchingItems($string, 'CONTAINS', $limit));
      }
    }
    // Allow other modules to alter the list of matches.
    $this->moduleHandler->alter('mpac_selection_matches', $matches, $type, $string);

    return array_slice($matches, 0, $limit, TRUE);
  }

}
