<?php

namespace Drupal\mpac\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Base class for selection plugins provided by Multi-path autocomplete.
 */
class SelectionBase implements DeriverInterface {

  use StringTranslationTrait;

  /**
   * Holds the list of plugin derivatives.
   *
   * @var array
   */
  protected $derivatives = [];

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinition($derivative_id, $base_plugin_definition) {
    if (!empty($this->derivatives) && !empty($this->derivatives[$derivative_id])) {
      return $this->derivatives[$derivative_id];
    }
    $this->getDerivativeDefinitions($base_plugin_definition);
    return $this->derivatives[$derivative_id];
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $supported_types = [
      'path',
      'shortcut',
    ];
    foreach (mpac_selection_plugin_info() as $type => $info) {
      if (!in_array($type, $supported_types)) {
        $this->derivatives[$type] = $base_plugin_definition;
        $this->derivatives[$type]['label'] = $this->t('@type selection', ['@type' => $info['label']]);
      }
    }
    return $this->derivatives;
  }

}
