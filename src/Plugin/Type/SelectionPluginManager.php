<?php

namespace Drupal\mpac\Plugin\Type;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Plugin type manager for the Multi-path autocomplete Selection plugin.
 */
class SelectionPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/mpac',
      $namespaces,
      $module_handler,
      'Drupal\mpac\Plugin\Type\SelectionInterface',
      'Drupal\mpac\Annotation\MpacSelection'
    );

    $this->alterInfo('mpac_selection');
    $this->setCacheBackend($cache_backend, 'mpac_selection_plugins');
  }

  /**
   * Overrides \Drupal\Component\Plugin\PluginManagerBase::getInstance().
   */
  public function getInstance(array $options) {
    $type = $options['type'];

    // Get all available selection plugins for this entity type.
    $selection_handler_groups = $this->getSelectionGroups($type);

    // Sort the selection plugins by weight and select the best match.
    uasort($selection_handler_groups, ['Drupal\Component\Utility\SortArray', 'sortByWeightElement']);
    end($selection_handler_groups);
    $plugin_id = key($selection_handler_groups);

    if ($plugin_id) {
      return $this->createInstance($plugin_id, $options);
    }

    return parent::getInstance($options);
  }

  /**
   * Returns a list of selection plugins that can provide autocomplete results.
   *
   * @param string $type
   *   A Multi-path autocomplete type.
   *
   * @return array
   *   An array of selection plugins grouped by selection group.
   */
  public function getSelectionGroups($type) {
    $plugins = [];

    foreach ($this->getDefinitions() as $plugin_id => $plugin) {
      if (!isset($plugin['types']) || in_array($type, $plugin['types'])) {
        $plugins[$plugin_id] = $plugin;
      }
    }

    return $plugins;
  }

}
