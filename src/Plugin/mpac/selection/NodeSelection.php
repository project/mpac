<?php

namespace Drupal\mpac\Plugin\mpac\selection;

use Drupal\node\Entity\Node;

/**
 * Provides specific selection functions for nodes.
 *
 * @MpacSelection(
 *   id = "node",
 *   module = "mpac",
 *   label = @Translation("Node selection"),
 *   types = {"*"},
 *   group = "default",
 *   weight = 10
 * )
 */
class NodeSelection extends SelectionBase {

  /**
   * {@inheritdoc}
   */
  public function countMatchingItems($match = NULL, $match_operator = 'CONTAINS') {
    $query = $this->buildEntityQuery($match, $match_operator);
    return $query
      ->count()
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getMatchingItems($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    if (!isset($match)) {
      return [];
    }

    $query = $this->buildEntityQuery($match, $match_operator);
    if ($limit > 0) {
      $query->range(0, $limit);
    }

    $result = $query->execute();

    if (empty($result)) {
      return [];
    }

    $matches = [];
    // Load entities.
    $entities = Node::loadMultiple($result);
    foreach ($entities as $entity_id => $entity) {
      $matches[] = [
        'value' => "/node/{$entity_id}",
        'label' => htmlspecialchars($entity->label()),
      ];
    }

    return $matches;
  }

  /**
   * Builds an EntityQuery to get matching nodes.
   *
   * @param string|null $match
   *   (Optional) Text to match the label against. Defaults to NULL.
   * @param string $match_operator
   *   (Optional) The operation the matching should be done with. Defaults
   *   to "CONTAINS".
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The EntityQuery object with the basic conditions applied to it.
   */
  private function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $target_type = 'node';
    $entity_info = $this->entityTypeManager->getDefinition($target_type);

    $query = $this->entityTypeManager->getStorage($target_type)->getQuery();
    $query->accessCheck();

    if (isset($match)) {
      $query->condition($entity_info->getKey('label'), $match, $match_operator);
    }

    // Adding the 'node_access' tag is sadly insufficient for nodes: core
    // requires us to also know about the concept of 'published' and
    // 'unpublished'. We need to do that as long as there are no access control
    // modules in use on the site. As long as one access control module
    // is there, it is supposed to handle this check.
    if (!$this->currentUser->hasPermission('bypass node access') && !count($this->moduleHandler->invokeAll('node_grants'))) {
      $query->condition('status', 1);
    }

    return $query;
  }

}
