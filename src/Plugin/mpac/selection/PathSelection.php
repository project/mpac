<?php

namespace Drupal\mpac\Plugin\mpac\selection;

use Drupal\Core\Database\Database;

/**
 * Provides specific selection functions for nodes.
 *
 * @MpacSelection(
 *   id = "path",
 *   module = "mpac",
 *   label = @Translation("Path selection"),
 *   types = {"path"},
 *   group = "default",
 *   weight = 1
 * )
 */
class PathSelection extends SelectionBase {

  /**
   * {@inheritdoc}
   */
  public function countMatchingItems($match = NULL, $match_operator = 'CONTAINS') {
    $query = $this->buildQuery($match, $match_operator);
    return $query
      ->count()
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getMatchingItems($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    if (!isset($match)) {
      return [];
    }

    $query = $this->buildQuery($match, $match_operator);
    $result = $query
      ->fields('path_alias')
      ->execute();

    if (empty($result)) {
      return [];
    }

    $matches = [];
    foreach ($result as $data) {
      $matches[] = [
        'value' => $data->path,
        'label' => sprintf('%s *', $data->alias),
      ];
    }

    return $matches;
  }

  /**
   * Helper to build query.
   */
  private function buildQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = Database::getConnection('default')->select('path_alias');
    if (isset($match)) {
      $query->condition('alias', '%' . $match . '%', 'LIKE');
    }
    return $query;
  }

}
